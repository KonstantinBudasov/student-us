function contentTabs() {
	var links = $('.js-tab-ln'),
		content = $('.js-tab-content'),
		activeClass = 'js-tab-ln_selected';

	content.hide();
	$(content[0]).show();
	$(links[0]).addClass(activeClass);

	links.on('click', function (e) {
		e.preventDefault();

		var self = $(this);

		if (!self.hasClass(activeClass)) {
			links.removeClass(activeClass);
			self.addClass(activeClass);

			content.hide();
			$(self.attr('href')).fadeIn(10);
			$('body, html').animate({scrollTop:self.offset().top}, 200);
		}
	});
}

function accordion() {
	var links = $('.js-collap-ln'),
		descriptions = $('.accordion-item-details'),
		activeClass = 'js-collap-ln_active';

	links.removeClass(activeClass);
	descriptions.hide();

	links.on('click', function() {
		var self = $(this);
		if (self.hasClass(activeClass)) {
			self.removeClass(activeClass).next(descriptions).slideUp();
		} else {
			links.removeClass(activeClass);
			self.addClass(activeClass);

			descriptions.slideUp();
			self.next(descriptions).slideDown();
		}
		return false;
	});

}

function slidingPanels() {
	var links = $('.sliding-panel-icon, .sliding-panel-content-header'),
		descriptions = $('.sliding-panel-content'),
		activeClass = 'active',
		clickToHide = $('.sliding-panel-content-header');

	links.removeClass(activeClass);
	descriptions.hide();

	links.on('click', function() {
		var self = $(this);
		if (self.hasClass(activeClass)) {
			self.removeClass(activeClass).prev(descriptions).fadeOut();
		} else {
			links.removeClass(activeClass);
			self.addClass(activeClass);

			descriptions.fadeOut(150);
			self.prev(descriptions).fadeIn(150);
		}
		return false;
	});

	$('.sliding-panel-content-header').on('click', function() {
		$(this).parent().fadeOut;
		return false;
	});

}

$( document ).ready(function() {

	// Tabs init
	contentTabs();

	// Accordion init
	accordion();

	// Sliding panels init
	slidingPanels();

	// Vertical slider
	$('#vertical-slider ul a').click(function () {
		//reset all the items
		$('#vertical-slider ul a').removeClass('active');
		//set current item as active
		$(this).addClass('active');
		//scroll it to the right position
		$('.mask').scrollTo($(this).attr('rel'), 300);
		//disable click event
		return false;
	});

	// Jstyling - custom form elements
	$.jStyling.createCheckbox($('input[type=checkbox]'));
	$.jStyling.createSelect($('select'));

	// jQuery placeholder
	$('input, textarea').placeholder();

});